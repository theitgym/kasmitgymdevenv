FROM kasmweb/core-ubuntu-bionic:1.10.0
USER root

ENV HOME /home/kasm-default-profile
ENV STARTUPDIR /dockerstartup
ENV INST_SCRIPTS $STARTUPDIR/install
WORKDIR $HOME

######### Customize Container Here ###########

# Install Google Chrome
COPY ./scripts/chrome $INST_SCRIPTS/chrome/
RUN bash $INST_SCRIPTS/chrome/chrome.sh  && rm -rf $INST_SCRIPTS/chrome/


### Install Visual Studio Code
COPY ./scripts/vscode $INST_SCRIPTS/vscode/
RUN bash $INST_SCRIPTS/vscode/vs_code.sh  && rm -rf $INST_SCRIPTS/vscode/


RUN curl -sL https://deb.nodesource.com/setup_17.x | bash - && \
apt-get install -yq nodejs build-essential

RUN npm install -g npm

RUN apt-get install -yq python3 python3-venv python3-pip


RUN echo "/usr/bin/desktop_ready && code --install-extension ritwickdey.liveserver && code --install-extension ecmel.vscode-html-css && code --install-extension tht13.html-preview-vscode && code --install-extension ms-python.python & "  > $STARTUPDIR/custom_startup.sh && chmod +x $STARTUPDIR/custom_startup.sh

######### End Customizations ###########

RUN chown 1000:0 $HOME
RUN $STARTUPDIR/set_user_permission.sh $HOME

ENV HOME /home/kasm-user
WORKDIR $HOME
RUN mkdir -p $HOME && chown -R 1000:0 $HOME

USER 1000
